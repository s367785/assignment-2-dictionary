%include 'lib.inc'
section .text

global find_word

find_word:
		push r12
		push r13
		mov r12, rdi
		mov r13, rsi
	.loop:
		test r13, r13
		jz .end_if_not_found
		lea rsi, [r13+8]
		mov rdi, r12
		call string_equals
		test rax, rax
		jnz .end_if_found
		mov r13, [r13]
		jmp .loop
	.end_if_not_found:
		xor rax, rax
		jmp .end
	.end_if_found:
		mov rax, r13
	.end:	
		pop r13
		pop r12
		ret
		
