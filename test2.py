import subprocess

keys = ['first_word', 'second_word', 'third_word', 'please', 'don`t', 'judge', 'strictly', 'help'*65, 'me'*130, 'a'*255, 'b'*256]
expected_output = ['first word explanation', 'second word explanation', 'third word explanation', '', '', '', '', '', '', '', '']
expected_error = ['', '', '', 'There is no such key in a dictionary', 'There is no such key in a dictionary', 'There is no such key in a dictionary', 'There is no such key in a dictionary', 'This key is too long', 'This key is too long', 'There is no such key in a dictionary', 'This key is too long']	

wrong = 0;

for i in range(len(keys)):
	process = subprocess.run("./program", input=keys[i], text=True, capture_output=True)
	
	if process.stdout != expected_output[i] or process.stderr != expected_error[i]:
		print("Test failed")
		wrong += 1
print("Number of wrong answers = ", wrong)
		
	
	
