%define element 0
%macro colon 2
	%ifstr %1
		%ifid %2
			%2:
				dq element
				db %1, 0
		%else
			%error "The key value must be an identifier"
		%endif
	%else
		%error "The value must be a string"
	%endif
	%define element %2
%endmacro
