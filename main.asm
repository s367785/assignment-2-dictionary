%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define BUF_SIZE 256

section .rodata
er: db "This key is too long", 0
res: db "There is no such key in a dictionary", 0

section .bss
buf: resb BUF_SIZE

section .text

global _start


_start:
		mov rdi, buf
		mov rsi, BUF_SIZE
		call read_word
		test rax, rax
		jz .end_if_long_key
		mov rdi, rax
		mov rsi, element
		call find_word
		test rax, rax
		jz .end_if_key_not_found
		add rax, 8
		add rax, rdx
		inc rax
		mov rdi, rax
		call print_string
		jmp .end
	.end_if_long_key:
		mov rdi, er
		jmp .err
	.end_if_key_not_found:
		mov rdi, res
	.err:
		call print_error
	.end:
		call exit
