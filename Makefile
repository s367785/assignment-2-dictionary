ASM = nasm
ASMFLAG = -f elf64
LD = ld
PY = python3
SOURCE = $(wildcard *.asm)
OBJECTS = $(SOURCE:.asm=.o)

%.o: %.asm
	$(ASM) $(ASMFLAG) -o $@ $<
	
main.o: main.asm lib.inc dict.inc colon.inc

dict.o: dict.asm lib.inc

program: $(OBJECTS)
	$(LD) -o $@ $^
	
test: program
	$(PY) test2.py
	
clean:
	rm -f *.o program
	
.PHONY: test clean

